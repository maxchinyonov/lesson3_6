<h3 class="code-line" data-line-start=0 data-line-end=1 ><a id="__0"></a>Описание задания</h3>
<p class="has-line-data" data-line-start="1" data-line-end="2">Описание задания</p>
<ol>
<li class="has-line-data" data-line-start="2" data-line-end="3">Создать 2 инстанса Ddebian CentOS</li>
<li class="has-line-data" data-line-start="3" data-line-end="9">Добавить в роль с прошлого задания таски которые в зависимости от ОС установит разные пакеты<br>
Debian - nmap<br>
CentOS - atop<br>
Также сменит тайм зону<br>
Debian - Tokyo<br>
CentOS - Moscow</li>
<li class="has-line-data" data-line-start="9" data-line-end="10">В конфиг нжингса (шаблон) добавить ИП адрес сервера из фактов.</li>
</ol>
<h3 class="code-line" data-line-start=12 data-line-end=13 ><a id="_12"></a>Решение</h3>
<p class="has-line-data" data-line-start="14" data-line-end="15"><a href="https://gitlab.com/maxchinyonov/lesson3_6">https://gitlab.com/maxchinyonov/lesson3_6</a></p>
<p class="has-line-data" data-line-start="16" data-line-end="17">Делал для трёх хостов. Было 2 дебиана с прошлых уроков и добавил центос.</p>
<h5 class="code-line" data-line-start=18 data-line-end=19 ><a id="2__18"></a>2. Пункт</h5>
<p class="has-line-data" data-line-start="19" data-line-end="21">Таски<br>
<a href="https://gitlab.com/maxchinyonov/lesson3_6/-/blob/master/roles/users_names_hosts/tasks/timezone_packages.yml">https://gitlab.com/maxchinyonov/lesson3_6/-/blob/master/roles/users_names_hosts/tasks/timezone_packages.yml</a></p>
<p class="has-line-data" data-line-start="22" data-line-end="24">Переменные для них<br>
<a href="https://gitlab.com/maxchinyonov/lesson3_6/-/tree/master/roles/users_names_hosts/vars">https://gitlab.com/maxchinyonov/lesson3_6/-/tree/master/roles/users_names_hosts/vars</a></p>
<p class="has-line-data" data-line-start="25" data-line-end="29">Создаём два файла с переменными в зависимости от ОС.<br>
В начале импортируем переменные из файлов. В зависимости от ОС хоста для которого выполняется плей подтягиваются разные переменные.<br>
Достигается тем, что имена файлов с переменными устанавливаем такими значениями, которые принимает переменная {{ansible_os_family}}<br>
Т.е для Дебиан переменные одни, для Центос другие.</p>
<p class="has-line-data" data-line-start="30" data-line-end="31">Для ОС CentOS в моём случае {{ansible_os_family}} = RedHat. Но не суть важно.</p>
<p class="has-line-data" data-line-start="32" data-line-end="35">Пакеты устанавливаем как обычно, имена пакетов подтягиваются из переменных в зависимости от ОС<br>
Добавил loop для случая, если пакетов будет больше одного.<br>
Таймзону задаём аналогично через переменные. loop тут не нужен.</p>
<h5 class="code-line" data-line-start=36 data-line-end=37 ><a id="3__36"></a>3. Пункт</h5>
<p class="has-line-data" data-line-start="37" data-line-end="40"><a href="https://gitlab.com/maxchinyonov/lesson3_6/-/blob/master/roles/nginx/templates/upstream.j2">https://gitlab.com/maxchinyonov/lesson3_6/-/blob/master/roles/nginx/templates/upstream.j2</a><br>
Добавляем строки используя ip адреса из фактов.<br>
Надо же было добавить все сервера и в апстримы ?</p>
<p class="has-line-data" data-line-start="41" data-line-end="44">Если добавил не туда, то в любом случае в основном конфиге в шаблоне строка бы выглядела как-то так.<br>
server_name {{ hostvars[item][‘ansible_facts’][‘default_ipv4’][‘address’] }};<br>
где item заданный в таске через loop был бы - ansible_play_hosts_all</p>
<p class="has-line-data" data-line-start="45" data-line-end="46">Апстримы ранее вынес в отдельный файл, потому что у нас по предыдущим урокам было по 2 конфига nginx’a с разными именами. При старте nginx ругался, что в обоих файлах одинаковые апстримы, т.к он цепляет все файлы из директории с окончанием .conf. Поэтому сделал единственным отдельным файлом.</p>
